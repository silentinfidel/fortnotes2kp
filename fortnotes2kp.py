#!/usr/bin/env python

import sys
import os
import getopt
import json
import gzip
import getpass
import hashlib
import re

from keepass import kpdb # pip install keepass


class Tag:
    def __init__(self, id):
        self.id = id
        self.uses = 0
        self.created_time = 0
        self.modified_time = 0
        self.name_jsol = None

    def load(self, tag_dict):
        self.uses = tag_dict['uses']
        self.created_time = tag_dict['ctime']
        self.modified_time = tag_dict['mtime']
        self.name_jsol = tag_dict['name']


class NoteEntry:
    def __init__(self, id):
        self.id = id

    def load(self, entry_dict):
        self.name_jsol = entry_dict['name']
        self.data_jsol = entry_dict['data']
        self.is_active = entry_dict['is_active'] != 0
        self.time = entry_dict['time']
        self.place = entry_dict['place']
        self.id_type = entry_dict['id_type']

class NoteTag:
    def __init__(self, time, tag):
        self.time = time
        self.tag = tag


class Note:
    def __init__(self, id):
        self.id = id
        self.is_active = False
        self.created_time = 0
        self.modified_time = 0
        #self.accessed_time = 0
        self.entries = []
        self.tags = set()

    def load(self, note_dict):
        self.is_active = note_dict['is_active'] != 0
        self.created_time = note_dict['ctime']
        self.modified_time = note_dict['mtime']
        #self.accessed_time= note_dict['atime']


class FortNotesBackup:
    def __init__(self):
        self.hash = None
        self.time = None
        self.version = None
        self.tags = {}
        self.notes = {}

    def load(self, path):
        with gzip.open(path) as gz:
            data = json.load(gz)
            info = data['info']
            self.hash = info['hash']
            self.time = info['time']
            self.version = info['version']
            if self.version != 1:
                raise Exception('Unknown file format version (expected 1)')

            for id_str, tag_dict in data['tags'].iteritems():
                tag = Tag(int(id_str))
                tag.load(tag_dict)
                self.tags[int(id_str)] = tag

            for id_str, note_dict in data['notes'].iteritems():
                note = Note(int(id_str))
                note.load(note_dict)
                self.notes[int(id_str)] = note

            for tag_dict in data['note_tags']:
                note = self.notes.get(tag_dict['id_note'])
                tag = self.tags.get(tag_dict['id_tag'])
                if note and tag:
                    note_tag = NoteTag(tag_dict['time'], tag)
                    note.tags.add(note_tag)

            for id_str, entry_dict in data['note_entries'].iteritems():
                note = self.notes.get(entry_dict['id_note'])
                if note:
                    entry = NoteEntry(int(id_str))
                    entry.load(entry_dict)
                    i = 0
                    for i, other_entry in enumerate(note.entries):
                        if entry.place < other_entry.place:
                            break
                    note.entries.insert(i, entry)


def sjcl_decode(jsol_str):
    # FortNotes uses the Stanford Javascript Crypto Library (SJCL)
    SJCL_REGEX = re.compile(r'\{iv:"([^"]*)",ks:(\d+),ts:(\d+),salt:"([^"]*)",ct:"([^"]*)"\}')
    import base64
    match = SJCL_REGEX.match(jsol_str)
    if not match:
        raise Exception('Invalid SJCL string')

    # FortNotes uses key size (ks) = 256 bits (32 bytes), tag size (ts) = 128 bits (16 bytes).
    iv, ks, ts, salt, ct = match.groups()
    # sjcl.json.encrypt() generates a random 128 bit (16 byte / 4 word) nonce (aka IV).
    # This takes 22 characters in base 64, or 24 characters including padding.
    padded_iv = iv + '=' * (4 - len(iv) % 4)
    bin_iv = base64.decodestring(padded_iv)
    # sjcl.misc.cachedPbkdf2() generates a random 64 bit (8 byte / 2 word) salt.
    # This takes 11 characters in base 64, or 12 characters including padding.
    padded_salt = salt + '=' * (4 - len(salt) % 4)
    bin_salt = base64.decodestring(padded_salt)
    # The ciphertext (ct) returned by sjcl.mode.ccm.encrypt() consists
    # of a the encrypted message followed by the 8-byte "tag" (MAC).
    padded_ct = ct + '=' * (4 - len(ct) % 4)
    bin_ct = base64.decodestring(padded_ct)
    return bin_iv, ks, ts, bin_salt, bin_ct[:-8], bin_ct[-8:]


def sjcl_decrypt_ccm(jsol_str, password, cache):
    from Crypto.Cipher import AES
    from Crypto.Util import Counter
    from Crypto.Protocol.KDF import PBKDF2
    from Crypto.Hash import HMAC, SHA256

    # NOTE: Counter with CBC-MAC (CCM) mode is defined in RFC 3610
    iv, ks, ts, salt, ciphertext, mac_tag = sjcl_decode(jsol_str)

    cache_key = (password, salt)
    if cache_key in cache:
        key = cache[cache_key]
    else:
        # The 256-bit AES key is generated using PBKDF2
        prf = lambda password, salt: HMAC.new(password, salt, digestmod=SHA256).digest()
        key = PBKDF2(password, salt, dkLen=32, prf=prf)
        cache[cache_key] = key

    # The length "L" of the length field in "iv" varies between 1 and 4 bytes.
    # This leaves room for up to (15-L) bytes for the nonce.
    # For example, if len(plaintext) <= 255 then L = 1 and
    # the nonce is then 13 bytes long.
    if len(ciphertext) > 255:
        raise Exception('Cipher text > 255 bytes not handled')
    aes = AES.new(key, mode=AES.MODE_CCM, mac_len=8, nonce=iv[:13])
    # FIXME: cleartext = aes.decrypt_and_verify(ciphertext, mac_tag)
    cleartext = aes.decrypt(ciphertext)
    return cleartext


def main():
    input_path = None
    output_path = None
    options, args = getopt.getopt(sys.argv[1:], 'i:o:', ['input=', 'output='])
    for option, option_arg in options:
        if option in ('-i', '--input'):
            input_path = option_arg
        elif option in ('-o', '--output'):
            output_path = option_arg

    if not input_path:
        print('Error: -i <input_path> is mandatory')
        return 1
    if not output_path:
        output_path = os.path.splitext(input_path)[0] + '.kdb'

    input_db = FortNotesBackup()
    input_db.load(input_path)

    clear_pass = getpass.getpass('Please enter your FortNotes password: ')
    hash_pass = hashlib.sha256(clear_pass).hexdigest()
    if hash_pass != input_db.hash:
        print('Error: incorrect password')
        return 3

    cache = {}
    output_db = kpdb.Database(masterkey=clear_pass)
    for note in input_db.notes.itervalues():
        if not note.is_active:  # skip deleted notes
            continue
        path = '/Internet'
        title = ''
        username = ''
        password = ''
        url = ''
        notes = ''
        for entry in note.entries:
            if not entry.is_active:  # skip deleted entries
                continue
            name = sjcl_decrypt_ccm(entry.name_jsol, clear_pass, cache)
            data = sjcl_decrypt_ccm(entry.data_jsol, clear_pass, cache)
            if name in ('title', 'site url') or title == '':
                title = data
            if name in ('site url', 'server address'):
                url = data
            elif name in ('username', 'skype name', 'icq number', 'email'):
                username = data
            elif name == 'password':
                password = data
            elif name == 'comments':
                notes = data
            elif name == 'database name':
                if data != '':
                    title += '/' + data
        output_db.add_entry(path, title or 'Untitled', username, password, url, notes)

    output_db.write(output_path)

    return 0

if __name__ == '__main__':
    main()

